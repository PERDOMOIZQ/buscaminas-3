class Sembrador {
    campo;
    campoCantidadMinas;
    tamaño;
    cantidadMinas;
    unaCasilla;


    constructor() {
        this.tamaño = 10;
        this.campo = Array();
        this.campoCantidadMinas = new Array();
        this.cantidadMinas = Math.round(Math.pow(this.tamaño, 2) * 0.1);
    }

    crearCampo() {
        for (let i = 0; i < this.tamaño; i++) {
            for (let j = 0; j < this.tamaño; j++) {
                if (!this.campo[i]) {
                    this.campo[i] = Array();
                }
                this.campo[i][j] = 0;
            }
        }
    }


    sembrarMinas() {

        while (this.cantidadMinas > 0) {
            let fila = Math.floor(Math.random() * this.tamaño);
            let columna = Math.floor(Math.random() * this.tamaño);

            if (this.campo[fila][columna] === 0) {
                this.campo[fila][columna] = 1;
                this.cantidadMinas--;
            }
        }
    }


    contarMinasAlrededor() {
        let cantidad = 0;
        for (let i = 0; i < this.tamaño; i++) {
            for (let j = 0; j < this.tamaño; j++) {
                if (!this.campoCantidadMinas[i]) {
                    this.campoCantidadMinas[i] = new Array();
                }
                this.campoCantidadMinas[i][j] = 0;
                if (i == 0) {
                    if (j == 0) {
                        cantidad = 0;
                        if (this.campo[i + 1][j + 1] == 1) {
                            cantidad++;
                        }
                        if (this.campo[i + 1][j] == 1) {
                            cantidad++;
                        }
                        if (this.campo[i][j + 1] == 1) {
                            cantidad++;
                        }
                        this.campoCantidadMinas[i][j] = cantidad;
                    }
                    if (j == this.tamaño - 1) {
                        cantidad = 0;
                        if (this.campo[i + 1][j - 1] == 1) {
                            cantidad++;
                        }
                        if (this.campo[i + 1][j] == 1) {
                            cantidad++;
                        }
                        if (this.campo[i][j - 1] == 1) {
                            cantidad++;
                        }
                        this.campoCantidadMinas[i][j] = cantidad;
                    }
                    cantidad = 0;
                    if (this.campo[i][j - 1] == 1 || this.campo[i + 1][j - 1] == 1 || this.campo[i + 1][j] == 1 || this.campo[i + 1][j + 1] == 1 || this.campo[i][j + 1] == 1) {
                        if (this.campo[i][j - 1] == 1) {
                            cantidad++;
                        }
                        if (this.campo[i + 1][j - 1] == 1) {
                            cantidad++;
                        }
                        if (this.campo[i + 1][j] == 1) {
                            cantidad++;
                        }
                        if (this.campo[i + 1][j + 1] == 1) {
                            cantidad++;
                        }
                        if (this.campo[i][j + 1] == 1) {
                            cantidad++;
                        }
                        this.campoCantidadMinas[i][j] = cantidad;
                    } else {
                        this.campoCantidadMinas[i][j] = cantidad;
                    }
                }
                if (i == this.tamaño - 1) {
                    if (j == 0) {
                        cantidad = 0;
                        if (this.campo[i - 1][j] == 1) {
                            cantidad++;
                        }
                        if (this.campo[i - 1][j + 1] == 1) {
                            cantidad++;
                        }
                        if (this.campo[i][j + 1] == 1) {
                            cantidad++;
                        }
                        this.campoCantidadMinas[i][j] = cantidad;
                    }
                    if (j == this.tamaño - 1) {
                        cantidad = 0;
                        if (this.campo[i - 1][j - 1] == 1) {
                            cantidad++;
                        }
                        if (this.campo[i - 1][j] == 1) {
                            cantidad++;
                        }
                        if (this.campo[i][j - 1] == 1) {
                            cantidad++;
                        }
                        this.campoCantidadMinas[i][j] = cantidad;
                    }
                    if (this.campo[i][j - 1] == 1 || this.campo[i - 1][j - 1] == 1 || this.campo[i - 1][j] == 1 || this.campo[i - 1][j + 1] == 1 || this.campo[i][j + 1] == 1) {
                        cantidad = 0;
                        if (this.campo[i][j - 1] == 1) {
                            cantidad++;
                        }
                        if (this.campo[i - 1][j - 1] == 1) {
                            cantidad++;
                        }
                        if (this.campo[i - 1][j] == 1) {
                            cantidad++;
                        }
                        if (this.campo[i - 1][j + 1] == 1) {
                            cantidad++;
                        }
                        if (this.campo[i][j + 1] == 1) {
                            cantidad++;
                        }
                        this.campoCantidadMinas[i][j] = cantidad;
                    } else {
                        cantidad = 0;
                        this.campoCantidadMinas[i][j] = cantidad;
                    }
                }
                if (j == 0 && i > 0 && i < this.tamaño - 1) {
                    cantidad = 0;
                    if (this.campo[i - 1][j] == 1 || this.campo[i - 1][j + 1] == 1 || this.campo[i][j + 1] == 1 || this.campo[i + 1][j + 1] == 1 || this.campo[i + 1][j] == 1) {
                        if (this.campo[i - 1][j] == 1) {
                            cantidad++;
                        }
                        if (this.campo[i - 1][j + 1] == 1) {
                            cantidad++;
                        }
                        if (this.campo[i][j + 1] == 1) {
                            cantidad++;
                        }
                        if (this.campo[i + 1][j + 1] == 1) {
                            cantidad++;
                        }
                        if (this.campo[i + 1][j] == 1) {
                            cantidad++;
                        }
                        this.campoCantidadMinas[i][j] = cantidad;
                    } else {
                        cantidad = 0;
                        this.campoCantidadMinas[i][j] = cantidad;
                    }
                }
                if (j == this.tamaño - 1 && i > 0 && i < this.tamaño - 1) {
                    cantidad = 0;
                    if (this.campo[i - 1][j] == 1 || this.campo[i - 1][j - 1] == 1 || this.campo[i][j - 1] == 1 || this.campo[i + 1][j - 1] == 1 || this.campo[i + 1][j] == 1) {
                        if (this.campo[i - 1][j] == 1) {
                            cantidad++;
                        }
                        if (this.campo[i - 1][j - 1] == 1) {
                            cantidad++;
                        }
                        if (this.campo[i][j - 1] == 1) {
                            cantidad++;
                        }
                        if (this.campo[i + 1][j - 1] == 1) {
                            cantidad++;
                        }
                        if (this.campo[i + 1][j] == 1) {
                            cantidad++;
                        }
                        this.campoCantidadMinas[i][j] = cantidad;
                    } else {
                        cantidad = 0;
                        this.campoCantidadMinas[i][j] = cantidad;
                    }
                }
                if (i != 0 && j != 0 && i != this.tamaño - 1 && j != this.tamaño - 1) {
                    this.campoCantidadMinas[i][j] = miSembrador.numeroMinas(i, j);
                }
            }
        }
    }

    numeroMinas(fila, columna) {
        let cantidad = 0;
        if (this.campo[fila + 1][columna + 1]) {
            cantidad++;
        }
        if (this.campo[fila + 1][columna]) {
            cantidad++;
        }
        if (this.campo[fila + 1][columna - 1]) {
            cantidad++;
        }
        if (this.campo[fila][columna + 1]) {
            cantidad++;
        }
        if (this.campo[fila][columna - 1]) {
            cantidad++;
        }
        if (this.campo[fila - 1][columna - 1]) {
            cantidad++;
        }
        if (this.campo[fila - 1][columna]) {
            cantidad++;
        }
        if (this.campo[fila - 1][columna + 1]) {
            cantidad++;
        }
        return cantidad;
    }




    set tamañoUsuario(valor) {
        if (valor < 4) {
            this.tamaño = 4;
        } else {
            this.tamaño = valor;
        }
    }

    set cantidadMinasUsuario(valor) {

        valor = Math.round(valor);


        if (valor < 1) {
            this.cantidadMinas = 2;
        } else {
            this.cantidadMinas = valor;
        }
    }
}

let miSembrador = new Sembrador();

miSembrador.tamañoUsuario = 10;
miSembrador.cantidadMinasUsuario = 20;
miSembrador.crearCampo();
//miSembrador.iniciarJuego();
miSembrador.sembrarMinas();
miSembrador.contarMinasAlrededor();